import 'dart:async';
import 'dart:developer';
import 'dart:html';
import 'dart:typed_data';

import 'package:chat_client/generated/message.pb.dart';
import 'package:riverpod/riverpod.dart';

import '../message_provider.dart';

/// провайдер подключения к веб-сокету
/// не использовать напрямую
/// провайдер используется только в провайдерах в файле web/handler
/// только для веб-приложения
final connectionProvider = Provider<WebSocket?>((ref) {
  final webSocket = WebSocket('wss://chat.flamebrier.softlynx.ru/ws');

  ref.onDispose(() {
    webSocket.close();
  });

  return webSocket;
});

/// провайдер для получения сообщений
/// не использовать напрямую
/// провайдер используется только в провайдере [messageProvider]
final webSocketListenProvider = StreamProvider<ChatMessage>((ref) {
  final controller = StreamController<ChatMessage>();
  final webSocket = ref.watch(connectionProvider);

  if (webSocket != null) {
    webSocket.onError.listen((event) {
      log(event.toString());
    });

    webSocket.onMessage.listen((event) async {
      final dataReader = FileReader();
      dataReader.readAsArrayBuffer(event.data);
      await for (final _ in dataReader.onLoad) {
        final data = dataReader.result;
        if (data is Uint8List) {
          final msg = ChatMessage.fromBuffer(data);
          controller.add(msg);
        }
      }
    });
  }

  ref.onDispose(() {
    controller.close();
  });

  return controller.stream;
});

/// провайдер для отправки сообщений
/// не использовать напрямую
final sendMessageListenProvider = Provider<StreamController<ChatMessage>>((ref) {
  final controller = StreamController<ChatMessage>();
  final webSocket = ref.watch(connectionProvider);

  if (!controller.isClosed && (webSocket != null)) {
    controller.stream.listen((ChatMessage msg) {
      try {
        webSocket.send(msg.writeToBuffer());
      } catch (e) {
        log("send message error: $e");
      }
    });
  }

  return controller;
});
