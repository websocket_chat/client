import 'dart:async';

// import 'package:chat_client/desktop/handler.dart' if (dart.library.html) 'package:chat_client/web/handler.dart';
import 'package:chat_client/web/handler.dart';

import 'package:chat_client/generated/message.pb.dart';
import 'package:riverpod/riverpod.dart';

/// хранение списка сообщений
/// не использовать
final messageListProvider = Provider<List<ChatMessage>>((ref) => []);

/// хранение индексов сообщений
/// используется в [ListView.custom] в параметре [findChildIndexCallback]
/// нужно чтобы сообщения, которые уже существовали, не перестраивались
/// при получении нового сообщения
final indexListProvider = Provider<Map<String, int>>((ref) => {});

/// провайдер для получения списка сообщений
/// возвращает [List<ChatMessage>]
final messageProvider = StreamProvider<List<ChatMessage>>((ref) {
  final controller = StreamController<List<ChatMessage>>();
  final list = ref.watch(messageListProvider);
  final indexes = ref.watch(indexListProvider);

  ref.listen(webSocketListenProvider, (previous, AsyncValue<ChatMessage> msg) {
    final value = msg.value;
    if (value != null) {
      list.add(value);
      controller.add(list);
      indexes[value.id] = list.length - 1;
    }
  }, fireImmediately: true);

  return controller.stream;
});

/// провайдер для отправки сообщений
/// используйте метод [add] для отправки
/// провайдер принимает объекты типа [ChatMessage]
final sendMessageProvider = Provider<StreamController<ChatMessage>>((ref) {
  final controller = ref.watch(sendMessageListenProvider);

  ref.onDispose(() {
    controller.close();
  });

  return controller;
});

final periodicProvider =
StreamProvider.autoDispose.family<int, Duration>((ref, duration) {
  final streamController = StreamController<int>();
  final timer = Timer.periodic(duration, (timer) {
    if (!streamController.isClosed) {
      streamController.add(timer.tick + 1);
    }
  });
  streamController.onListen = () {
    if (!streamController.isClosed) {
      streamController.add(0);
    }
  };
  ref.onDispose(() {
    timer.cancel();
    streamController.close();
  });
  return streamController.stream;
});