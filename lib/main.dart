import 'dart:math';
import 'package:chat_client/generated/message.pb.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'message_provider.dart';

void main() {
  runApp(const ProviderScope(child: ChatApp()));
}

class ChatApp extends StatefulWidget {
  const ChatApp({Key? key}) : super(key: key);

  @override
  _ChatAppState createState() => _ChatAppState();
}

class _ChatAppState extends State<ChatApp> {
  late MaterialColor primary;

  @override
  void initState() {
    super.initState();
    primary =
        Colors.primaries.elementAt(Random().nextInt(Colors.primaries.length));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat Room',
      theme: ThemeData(
        primarySwatch: primary,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends ConsumerStatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  ConsumerState<MyHomePage> createState() => MyHomePageState();
}

class MyHomePageState extends ConsumerState<MyHomePage> {
  final String userId = const Uuid().v4();

  TextEditingController nicknameController = TextEditingController();
  TextEditingController textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final sendController = ref.watch(sendMessageProvider);
    final messageList = ref.watch(messageProvider);
    final indexes = ref.watch(indexListProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Chat Room"),
        actions: [Text("Сообщений в чате: ${indexes.length}")],
      ),
      body: Center(
          child: Container(
        width: MediaQuery.of(context).size.width * 0.75,
        constraints: const BoxConstraints(minWidth: 400, maxWidth: 700),
        child: Column(children: [
          Expanded(
            child: messageList.when(
                data: (messages) {
                  return ListView.custom(
                      padding: const EdgeInsets.only(top: 30, bottom: 30),
                      childrenDelegate: SliverChildBuilderDelegate(
                          (context, index) {
                            final ChatMessage msg = messages[index];
                            final time = DateTime.fromMillisecondsSinceEpoch(
                                msg.epochMilliseconds.toInt());

                            final color = Color(msg.user.color);

                            if (msg.user.id == userId) {
                              return Padding(
                                key: ValueKey<String>(msg.id),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text.rich(TextSpan(
                                          text: msg.user.nickname,
                                          children: [
                                            TextSpan(
                                                text: " (Вы)",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .overline)
                                          ],
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle2!
                                              .copyWith(color: color))),
                                      Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 4.0),
                                              child: Text(
                                                  "${time.hour}:${time.minute}",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .caption),
                                            ),
                                            Container(
                                              constraints: BoxConstraints(
                                                  maxWidth: min(
                                                      MediaQuery.of(context)
                                                              .size
                                                              .width *
                                                          0.5,
                                                      600)),
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 4,
                                                      vertical: 2),
                                              decoration: BoxDecoration(
                                                  color: Theme.of(context)
                                                      .primaryColorLight,
                                                  borderRadius:
                                                      const BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  15),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  15),
                                                          topRight:
                                                              Radius.circular(
                                                                  15))),
                                              child: Text(msg.text),
                                            ),
                                          ]),
                                    ]),
                              );
                            }

                            return Padding(
                              key: ValueKey<String>(msg.id),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 4),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(msg.user.nickname,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2!
                                            .copyWith(color: color)),
                                    Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Container(
                                            constraints: BoxConstraints(
                                                maxWidth: min(
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width *
                                                        0.5,
                                                    600)),
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 4, vertical: 2),
                                            decoration: BoxDecoration(
                                                color: Colors.grey.shade300,
                                                borderRadius:
                                                    const BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(15),
                                                        bottomRight:
                                                            Radius.circular(15),
                                                        topRight:
                                                            Radius.circular(
                                                                15))),
                                            child: Text(msg.text),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 4.0),
                                            child: Text(
                                                "${time.hour}:${time.minute}",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .caption),
                                          ),
                                        ]),
                                  ]),
                            );
                          },
                          childCount: messages.length,
                          findChildIndexCallback: (Key key) {
                            if (key is ValueKey<String>) {
                              final String data = key.value;
                              return indexes[data];
                            }
                            return null;
                          }));
                },
                error: (e, s) {
                  return Text(e.toString());
                },
                loading: () => Container()),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(15, 5, 15, 5),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: 200,
                    clipBehavior: Clip.antiAlias,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(15)),
                    child: TextField(
                        controller: nicknameController,
                        maxLines: 1,
                        inputFormatters: [LengthLimitingTextInputFormatter(20)],
                        decoration: InputDecoration(
                            hintText: "Никнейм",
                            filled: true,
                            fillColor: Colors.grey.shade50,
                            border: InputBorder.none)),
                  ),
                  Container(
                    clipBehavior: Clip.antiAlias,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            topRight: Radius.circular(15))),
                    child: TextField(
                        controller: textController,
                        minLines: 3,
                        maxLines: 7,
                        decoration: InputDecoration(
                            hintText: "Введите сообщение...",
                            filled: true,
                            fillColor: Colors.grey.shade200,
                            border: InputBorder.none)),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(15),
                                bottomRight: Radius.circular(15)))),
                    onPressed: () {
                      sendController.add(ChatMessage(
                          id: userId,
                          user: User(
                              id: userId,
                              nickname: nicknameController.text,
                              color: Theme.of(context).primaryColorDark.value),
                          text: textController.text));
                      textController.clear();
                    },
                    child: const Center(child: Text("Отправить")),
                  ),
                ]),
          )
        ]),
      )),
    );
  }
}
